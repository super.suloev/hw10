#ifndef HW10_MAIN_H
#define HW10_MAIN_H

#include <stdio.h>
#include <stdlib.h>

#include "data.h"
#include "functions.h"

int main(int argc, char *argv[]);

#endif
