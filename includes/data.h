#ifndef HW10_DATA_H
#define HW10_DATA_H

typedef struct Node_t {
    long value;
    struct Node_t *next_node_P;
} Node_t;

typedef struct {
    long N, M, min, max, num;
    struct Node_t *first_node_P;
} Data_t;

#endif
