#ifndef HW10_FUNCTIONS_H
#define HW10_FUNCTIONS_H

#include "data.h"
#include <stdio.h>
#include <stdlib.h>

Data_t *parse_data(FILE *fp, long N, long M);

void sort_and_print(Data_t *data_P);

#endif
