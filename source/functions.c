#include "../includes/functions.h"

Data_t *parse_data(FILE *fp, long N, long M) {
    Data_t *data_P;
    data_P = (Data_t *) malloc(sizeof(Data_t));
    data_P->num = 1;
    data_P->N = N;
    data_P->M = M;

    Node_t *this_node_P;
    this_node_P = malloc(sizeof(Node_t));
    this_node_P->next_node_P = this_node_P; // Random pointer (not NULL)
    data_P->first_node_P = this_node_P;

    long this_value;
    while (fscanf(fp, "%ld\n", &this_value) == 1) {
        if (this_value >= N && this_value <= M) {
            this_node_P->value = this_value;
            this_node_P->next_node_P = NULL;
            data_P->min = this_value;
            data_P->max = this_value;
            break;
        }
    }

    if (this_node_P->next_node_P != NULL) {
        printf("Numbers that more then %ld and less then %ld not found", N, M);
        exit(0);
    }

    while (fscanf(fp, "%ld\n", &this_value) == 1) {
        if (this_value >= N && this_value <= M) {
            Node_t *new_node_P;
            new_node_P = (Node_t*) malloc(sizeof(Node_t));
            new_node_P->value = this_value;
            new_node_P->next_node_P = NULL;
            if (this_value > data_P->max)
                data_P->max = this_value;
            if (this_value < data_P->min)
                data_P->min = this_value;
            ++data_P->num;
            this_node_P->next_node_P = new_node_P;
            this_node_P = new_node_P;
        }
    }

    return data_P;
}


void sort_and_print(Data_t *data_P) {
    Node_t *this_node_P;
    this_node_P = data_P->first_node_P;

    long array[data_P->max - data_P->min + 1];
    for (long i = 0; i < data_P->max - data_P->min + 1; ++i)
        array[i] = 0;

    while (1) {
        ++array[this_node_P->value - data_P->min];
        if (this_node_P->next_node_P != NULL)
            this_node_P = this_node_P->next_node_P;
        else
            break;
    }

    FILE *fp;
    fp = fopen("../output.txt", "w");

    for (long i = 0; i < data_P->max - data_P->min + 1; ++i)
        if (array[i] != 0)
            for (long j = 0; j < array[i]; ++j) {
                printf("%ld ", i + data_P->min);
                fprintf(fp, "%ld ", i + data_P->min);
            }

    fclose(fp);
}