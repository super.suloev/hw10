#include "includes/main.h"

int main(int argc, char *argv[]) {
    long M, N;
    N = atol(argv[2]);
    M = atol(argv[3]);
    FILE *fp;
    fp = fopen(argv[1], "r");
    Data_t *data_P;
    data_P = parse_data(fp, N, M);
    fclose(fp);

    sort_and_print(data_P);

    return 0;
}
